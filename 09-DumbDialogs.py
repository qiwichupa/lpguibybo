#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys

__appname__ = "DumbDialogs"


class Program(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        self.setWindowTitle(__appname__)

        btn = QPushButton("Open dialog")
        self.label1 = QLabel("Label 1 Result")
        self.label2 = QLabel("Label 2 Result")

        layout = QVBoxLayout()
        layout.addWidget(btn)
        layout.addWidget(self.label1)
        layout.addWidget(self.label2)

        self.setLayout(layout)

        #self.connect(btn, SIGNAL("clicked()"), self.dialogOpen)
        btn.clicked.connect(self.dialogOpen)

    def dialogOpen(self):
        dialog = Dialog()
        if dialog.exec_():
            self.label1.setText("Spinbox value is " + str(dialog.spinbox.value()))
            self.label2.setText("Checkbox is " + str(dialog.checkbox.isChecked()))
        else:
            QMessageBox.warning(self, __appname__, "Dialog canceled")
            self.label1.setText("canceled")
            self.label2.setText("canceled")


class Dialog(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        self.setWindowTitle("Dialog")

        self.checkbox = QCheckBox("Check me")
        self.spinbox = QSpinBox()
        buttonOk = QPushButton("OK")
        buttonCancel = QPushButton("Cancel")

        layout = QGridLayout()
        layout.addWidget(self.checkbox, 0, 1)
        layout.addWidget(self.spinbox, 0, 0)
        layout.addWidget(buttonOk, 1, 0)
        layout.addWidget(buttonCancel, 1, 1)

        self.setLayout(layout)

        #self.connect(buttonOk, SIGNAL("clicked()"), self, SLOT("accept()"))
        #self.connect(buttonCancel, SIGNAL("clicked()"), self, SLOT("reject()"))
        buttonOk.clicked.connect(self.accept)
        buttonCancel.clicked.connect(self.reject)


app = QApplication(sys.argv)
form = Program()
form.show()
app.exec_()

