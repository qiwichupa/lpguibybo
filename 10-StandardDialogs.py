#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import sys

__appname__ = "StandardDialogs"


class Program(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        self.setWindowTitle(__appname__)

        btn = QPushButton("Open dialog")
        self.mainSpinBox = QSpinBox()
        self.mainCheckBox = QCheckBox("Main checkbox value")

        layout = QVBoxLayout()

        layout.addWidget(self.mainSpinBox)
        layout.addWidget(self.mainCheckBox)
        layout.addWidget(btn)

        self.setLayout(layout)

        btn.clicked.connect(self.dialogOpen)

    def dialogOpen(self):
        initValues = {"mainSpinBox": self.mainSpinBox.value(), "mainCheckBox": self.mainCheckBox.isChecked()}
        dialog = Dialog(initValues)
        if dialog.exec_():
            self.mainSpinBox.setValue(dialog.spinbox.value())
            self.mainCheckBox.setChecked(dialog.checkbox.isChecked())


class Dialog(QDialog):

    def __init__(self, initValues, parent=None):
        QDialog.__init__(self)

        self.setWindowTitle("Dialog")

        self.checkbox = QCheckBox("Check me")
        self.spinbox = QSpinBox()
        buttonOk = QPushButton("OK")
        buttonCancel = QPushButton("Cancel")

        layout = QGridLayout()
        layout.addWidget(self.checkbox, 0, 1)
        layout.addWidget(self.spinbox, 0, 0)
        layout.addWidget(buttonOk, 1, 0)
        layout.addWidget(buttonCancel, 1, 1)

        self.setLayout(layout)

        self.spinbox.setValue(initValues["mainSpinBox"])
        self.checkbox.setChecked(initValues["mainCheckBox"])

        buttonOk.clicked.connect(self.accept)
        buttonCancel.clicked.connect(self.reject)

    def accept(self):

        class GreatherThanFive(Exception):
            pass

        class IsZero(Exception):
            pass

        try:
            if self.spinbox.value() > 5:
                raise GreatherThanFive("The spinbox value must be lower than 6")
            elif self.spinbox.value() == 0:
                raise IsZero("The spinbox value cannot be equal to 0")
            else:
                QDialog.accept(self)

        except GreatherThanFive as e:
            QMessageBox.warning(self, __appname__, str(e))
            self.spinbox.selectAll()
            self.spinbox.setFocus()
            return

        except IsZero as e:
            QMessageBox.warning(self, __appname__, str(e))
            self.spinbox.selectAll()
            self.spinbox.setFocus()
            return


app = QApplication(sys.argv)
form = Program()
form.show()
app.exec_()

