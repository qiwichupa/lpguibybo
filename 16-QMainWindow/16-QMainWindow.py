#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys

import mainGui


class MainWindow(QMainWindow, mainGui.Ui_MainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self)
        self.setupUi(self)

        self.actionExit.triggered.connect(self.exitApp)

    def exitApp(self):
        sys.exit(0)


app = QApplication(sys.argv)
form = MainWindow()
form.show()
app.exec_()
