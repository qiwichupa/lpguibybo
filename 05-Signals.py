#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys


class Form(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        self.dial = QDial()
        self.dial.setNotchesVisible(True)

        self.spinbox = QSpinBox()

        layout = QVBoxLayout()
        layout.addWidget(self.dial)
        layout.addWidget(self.spinbox)
        self.setLayout(layout)

        self.dial.valueChanged.connect(self.spinbox.setValue)
        self.spinbox.valueChanged.connect(self.dial.setValue)

        self.dial.valueChanged.connect(self.print_value)

    def print_value(self, value):
        print("the value is {0}".format(value))


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
