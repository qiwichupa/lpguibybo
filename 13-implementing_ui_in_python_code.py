#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import sys
import showGui


class MainDialog(QDialog, showGui.Ui_MainDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)
        self.setupUi(self)

        self.showButton.clicked.connect(self.showMessageBox)

    def showMessageBox(self):
        QMessageBox.information(self, "Hello!", "Hello there, " + self.nameEdit.text())


app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
