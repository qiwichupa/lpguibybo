#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys

import urllib.request


class Form(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        date = self.get_data()
        rates = sorted(self.rates.keys())

        dateLabel = QLabel(date)

        self.fromComboBox = QComboBox()
        self.toComboBox = QComboBox()

        self.fromComboBox.addItems(rates)
        self.toComboBox.addItems(rates)

        self.fromSpinbox = QDoubleSpinBox()
        self.fromSpinbox.setRange(0.01, 1000)
        self.fromSpinbox.setValue(1.00)

        self.toLabel = QLabel("1.00")

        layout = QGridLayout()
        layout.addWidget(dateLabel, 0, 0)
        layout.addWidget(self.fromComboBox, 1, 0)
        layout.addWidget(self.toComboBox, 2, 0)
        layout.addWidget(self.fromSpinbox, 1, 1)
        layout.addWidget(self.toLabel, 2, 1)
        self.setLayout(layout)

        self.fromComboBox.currentIndexChanged.connect(self.update_ui)
        self.toComboBox.currentIndexChanged.connect(self.update_ui)
        self.fromSpinbox.valueChanged.connect(self.update_ui)

    def get_data(self):
        self.rates = {}

        try:
            date = "Unknown"

            with urllib.request.urlopen("http://www.collectionscanada.gc.ca/eppp-archive/100/201/301/bank_can_review/2006/spring/cover/en/markets/csv/exchange_eng.csv") as fn:
                for line in fn.readlines():

                    line = line.rstrip().decode('utf-8')
                    if not line or line.startswith(("$", "#", "Closing")):
                        continue
                    fields = line.split(",")
                    if line.startswith("Date "):
                        date = fields[-1]
                    else:
                        try:
                            value = float(fields[-1])
                            self.rates[fields[0]] = value
                        except ValueError:
                            pass

            return "Exchange rates date: " + date
        except Exception as e:
            return("Failed to download: {0}".format(e))

    def update_ui(self):

        from_ = self.fromComboBox.currentText()
        to_ = self.toComboBox.currentText()

        results = (self.rates[from_] / self.rates[to_]) * self.fromSpinbox.value()

        self.toLabel.setText("{:0.2f}".format(results))


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
