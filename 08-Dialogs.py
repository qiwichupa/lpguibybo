#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys

__appname__ = "Dialogs"


class Program(QDialog):
    
    def __init__(self, parent=None):
        QDialog.__init__(self)
        
        openButton = QPushButton("Open")
        saveButton = QPushButton("Save")
        dirButton = QPushButton("Other")
        closeButton = QPushButton("Close...")
        
        openButton.clicked.connect(self.open)
        saveButton.clicked.connect(self.save)
        
        layout = QVBoxLayout()
        layout.addWidget(openButton)
        layout.addWidget(saveButton)
        layout.addWidget(dirButton)
        layout.addWidget(closeButton)
        
        self.setLayout(layout)
        
        
    def open(self):
        
        dir = "."
        
        fileObj = QFileDialog.getOpenFileName(self, __appname__ + " Open file...", dir=dir, filter="Python files (*.py)")
        print(fileObj)
        print(type(fileObj))
        
        filename = fileObj[0]
        
        file = open(filename, "r")
        read = file.read()
        file.close()
        print(read)
        
    def save(self):
        dir = "."
        
        fileObj = QFileDialog.getSaveFileName(self, __appname__, dir = dir, filter="Text files (*.txt)")
        print(fileObj)
        
        content = "Test file content"
        filename = fileObj[0]
        open(filename, "w").write(content)
        
        
        

app = QApplication(sys.argv)
form = Program()
form.show()
app.exec_()