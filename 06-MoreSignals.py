#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys


class ZeroSpinBox(QSpinBox):

    zeros = 0
    atZero = Signal(int, int)

    def __init__(self, parent=None):
        QSpinBox.__init__(self)

        self.valueChanged.connect(self.check_zero)

    def check_zero(self, value):
        if value == 0:
            self.zeros += 1
            self.constant = 5
            self.atZero.emit(self.zeros, self.constant)


class Form(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        self.dial = QDial()
        self.dial.setNotchesVisible(True)

        self.spinbox = ZeroSpinBox()

        layout = QVBoxLayout()
        layout.addWidget(self.dial)
        layout.addWidget(self.spinbox)
        self.setLayout(layout)

        self.dial.valueChanged.connect(self.spinbox.setValue)
        self.spinbox.valueChanged.connect(self.dial.setValue)

        self.spinbox.atZero.connect(self.print_value)

    def print_value(self, zeros, constant):
        print("the SpinBox has been at zero {0} times".format(zeros))
        print("Constant is {0}".format(constant))


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
