from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Preferences(QDialog):

    checkboxSig = Signal(bool)

    def __init__(self, parent=None, showToolBar=True):
        QDialog.__init__(self)

        self.resize(200, 200)
        self.setWindowTitle("Preferences")

        self.checkBox = QCheckBox("Show main toolbar")
        self.checkBox.setChecked(showToolBar)

        closeBtn = QPushButton("Close")

        layout = QVBoxLayout()
        layout.addWidget(self.checkBox)
        layout.addWidget(closeBtn)

        self.setLayout(layout)

        self.checkBox.stateChanged.connect(self.checkBoxStateChanged)
        closeBtn.clicked.connect(self.close)

    def checkBoxStateChanged(self):
        self.checkboxSig.emit(self.checkBox.isChecked())



