#!/usr/bin/env python3.7

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import sys
import time
import showGui


class MainDialog(QDialog, showGui.Ui_MainDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)
        self.setupUi(self)

        self.showButton.setText("Process")

        self.showButton.clicked.connect(self.processData)

        self.workerThread = WorkerThread()

        self.connect(self.workerThread, SIGNAL("threadDone(QString, QString)"), self.threadDone)

    def processData(self):
        self.workerThread.start()
        QMessageBox.information(self, "Done!", "Done.")

    def threadDone(self, text, text2):
        #QMessageBox.warning(self, "Warning!", "Thread execution completed.")
        self.nameEdit.setText("Worker thread finished processing.")
        print(text)
        print(text2)


class WorkerThread(QThread):

    def __init__(self, parent=None):
        QThread.__init__(self)

    def run(self):
        time.sleep(3)
        #print("Done with the thread")

        self.emit(SIGNAL("threadDone(QString, QString)"), "Confirmation that the thread is finished", "Another confirmation that the thread has been completed")


app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
