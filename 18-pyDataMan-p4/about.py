from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class About(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self)

        text = QTextEdit()
        text.setPlainText("This is PyDataMan!\r\nSoftware released in 2XXX.")
        text.setReadOnly(True)

        btnClose = QPushButton("Close")

        layout = QVBoxLayout()
        layout.addWidget(text)
        layout.addWidget(btnClose)
        self.setLayout(layout)

        btnClose.clicked.connect(self.close)


